
# Плагин для вывода похожих материалов для AtomM CMS 5

Плагин на основе ключевых слов материала проводит поиск по сайту и выводит подобные по теме результаты.
Блок с результатами выводится меткой {{ relmat }}

Поиск производится по поисковому индексу сайта, причём не только среди ключевых слов, но и всего поискового индекса: это и заголовок, и описание, и ключевые слова.

В админке можно сменить дизайн для блока, указать максимальное количество выводимых результатов и выбрать, искать только в активном модуле или во всех, задать отдельные настройки для размера генерируемых миниатюр.

## Установка:
Плагин можно установить автоматически в каталоге плагинов (Админка->Плагины->Установка плагинов).

### Установка вручную:

1. Распаковать архив и переместить папку *relmat* в *plugins*.
2. Для файлов *plugins/relmat/config.json* и *plugins/relmat/template/index.html* выставить права 777.
3. Добавить метку *{{ relmat }}* в нужном месте шаблона.

## Шэф, усё пропало!
Если метка правильно установлена, но список похожих материалов не выводится, проверь:

1. В просматриваемом материале есть теги
2. По этим тегам можно что то найти (скопировав их в поле поиска по сайту)

Если всё равно ничего не вводится то обращайтесь на форум atom-m.net