<?php

class relmat {

    public function common($markers_data) {

        // Пропускаем, если в шаблоне отсутсвует метка
        if (isset($markers_data['relmat'])) return $markers_data;

        $Register = Register::getInstance();
        $this->DB = getDB();
        $this->Entity = $Register['current_vars'];
        $this->config = json_decode(file_get_contents(dirname(__FILE__).'/config.json'), true);

        $obj = $this;
        $markers_data['relmat'] = function() use($obj,$Register) {

            $obj->Params = $Register['params'];
            $cont = '';
            if (is_array($obj->Entity->getTags())) {

                $materials = $obj->search($obj->Params[0], $obj->Entity->getTags(), $obj->Entity->getId());
                if (count($materials) > 0) {
                    $template = file_get_contents(dirname(__FILE__).'/template/index.html');
                    $Viewer = new Viewer_Manager;
                    foreach ($materials as $key => $material) {
                        $Model = OrmManager::getModelInstance($material['module']);
                        $Model->bindModel('attaches', array(), array('module' => $material['module']));
                        $entity = $Model->getById($material['entity_id']);
                        if (empty($entity) || $entity == false) continue;

                        $markers['url'] = get_url(matUrl($material['entity_id'], $entity->getTitle(), $material['module']));
                        $markers['module'] = $material['module'];

                        $attach = $entity->getAttaches();
                        if (!empty($attach) && count($attach) > 0) {
                            if ($attach[0]->getIs_image() == '1') {
                                $markers['img'] = $this->smallImage($attach[0]->getFilename(), $material['module']);
                            }
                        }

                        $entity->setAtom($markers);
                        $materials[$key] = $entity;
                    }
                    $cont = $Viewer->parseTemplate($template, array('materials' => $materials));
                }
            }

            return $cont;
        };

        return $markers_data;
    }

    public function getImagesPath($file = null, $module = null, $size_x = null, $size_y = null)
    {
        if (!isset($module)) $module = $this->module;
        $path = '/data/images/' . $module . '/' . (($size_x !== null and $size_y !== null) ? $size_x . 'x' . $size_y . '/' : '') . (!empty($file) ? $file : '');
        return $path;
    }

    public function smallImage($filename, $module)
    {
        // Ищем настройки миниатюр в конфигурации плагина
        if (isset($this->config['use_local_preview']) and $this->config['use_local_preview']) {
            $preview = $this->config['use_preview'];
            $size_x = $this->config['img_size_x'];
            $size_y = $this->config['img_size_y'];
        // Иначе в конфигурации модуля
        } elseif (Config::read('use_local_preview', $module)) {
            $preview = Config::read('use_preview', $module);
            $size_x = Config::read('img_size_x', $module);
            $size_y = Config::read('img_size_y', $module);
        // Если опять пусто, то основкой конфигурации сайта
        } else {
            $preview = Config::read('use_preview');
            $size_x = Config::read('img_size_x');
            $size_y = Config::read('img_size_y');
        }
        $image_link = get_url($this->getImagesPath($filename, $module));
        $preview_link = $image_link;

        if ($preview) {
            if (file_exists(R.'/'.$this->getImagesPath($filename, $module, $size_x, $size_y))) {
                $preview_link = get_url($this->getImagesPath($filename, $module, $size_x, $size_y));
            } else {
                // Узнаем, а нужно ли превью для изображения
                list($width, $height, $type, $attr) = @getimagesize(R . $image_link);
                if ((empty($width) and empty($height)) or ($width > $size_x or $height > $size_y)) {
                    $preview_link = get_url($this->getImagesPath($filename, $module, $size_x, $size_y));
                }
            }
        }

        return $preview_link;
    }

    public function search($module, $tags, $id) {
        $string = resc(implode('* ', $tags) . '*');

        $sql = "
            SELECT DISTINCT entity_id,module FROM `" . $this->DB->getFullTableName('search_index') . "`
            WHERE MATCH (`index`) AGAINST ('" . $string . "' IN BOOLEAN MODE) AND entity_id != '".$id."'";
        if ($this->config['only_active_module']) {
            $sql .= " AND module = '".$module."'";
        } else {
            $sql .= " AND module != 'forum'";
        }

        // Without comments
        $sql .= " AND entity_table != 'comments'";
        $sql .= " ORDER BY MATCH (`index`) AGAINST ('" . $string . "' IN BOOLEAN MODE) DESC LIMIT " . $this->config['limit'];

        $resultsrel = $this->DB->query($sql);
        return $resultsrel;
    }
}

?>